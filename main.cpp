
// Copyright 2019 Adam Campbell, Seth Hall, Andrew Ensor
// Copyright 2019 High Performance Computing Research Laboratory, Auckland University of Technology (AUT)

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:

// 1. Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.

// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution.

// 3. Neither the name of the copyright holder nor the names of its
// contributors may be used to endorse or promote products derived from this
// software without specific prior written permission.

// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include <cstdlib>
#include <cstdio>
#include <ctime>
#include <sys/time.h>

#include "gain_calibration.h"

int main(int argc, char **argv)
{
	printf("====================================================================\n");
	printf(">>> AUT HPC Research Laboratory - GAIN GALIBRATION (GPU VERSION) <<<\n");
	printf("====================================================================\n\n");

	if(SINGLE_PRECISION)
		printf(">>> INFO: Executing GC using single precision...\n\n");
	else
		printf(">>> INFO: Executing GC using double precision...\n\n");

	// Seed random from time
	srand(time(NULL));

	Config config;
	init_config(&config);

	Complex *vis_measured = NULL;
	load_visibilities(&config, &vis_measured, true);

	if(vis_measured == NULL)
	{	
		printf(">>> ERROR: Visibility memory (MEASURED) was unable to be allocated...\n\n");
		if(vis_measured)       free(vis_measured);
		return EXIT_FAILURE;
	}


	Complex *vis_predicted= NULL;
	load_visibilities(&config, &vis_predicted, false);

	if(vis_predicted == NULL)
	{	
		printf(">>> ERROR: Visibility memory (PREDICTED) was unable to be fallocated...\n\n");
		if(vis_predicted)       free(vis_predicted);
		return EXIT_FAILURE;
	}

	//create gains_array and predicted
	Complex *gains_array = (Complex*) calloc(config.num_recievers, sizeof(Complex));

	for(int i=0;i<config.num_recievers;++i)
	{
		gains_array[i] = (Complex){ .real = 1.0, .imaginary = 0.0 };

		printf("measured >>%f and %f\n",vis_measured[i].real, vis_measured[i].imaginary);
		printf("predicted >>%f and %f\n",vis_predicted[i].real, vis_predicted[i].imaginary);
	}



	//SHALL WE STORE BASELINES IN CONFIG????
	int baselines = (config.num_recievers*(config.num_recievers-1))/2;

	int2 *receiver_pairs = (int2*) calloc(baselines, sizeof(int2));

	calculate_receiver_pairs(&config, receiver_pairs);

	execute_gain_calibration(&config, vis_measured, vis_predicted, gains_array, receiver_pairs);

	printf("UPDATE >> PRINTING ROTATED CALIBRATIONS FOR EACH OF THE %d RECEIVERS....\n\n ",config.num_recievers);


	Complex rotationZ = (Complex){
		.real = gains_array[0].real/SQRT(gains_array[0].real*gains_array[0].real + gains_array[0].imaginary * gains_array[0].imaginary), 
		.imaginary = -gains_array[0].imaginary/SQRT(gains_array[0].real*gains_array[0].real + gains_array[0].imaginary * gains_array[0].imaginary)
	};



	for(int i = 0; i<config.num_recievers; ++i)
	{	Complex rotatedGain = (Complex){
			.real = gains_array[i].real * rotationZ.real - gains_array[i].imaginary * rotationZ.imaginary,
			.imaginary =  gains_array[i].real * rotationZ.imaginary + gains_array[i].imaginary * rotationZ.real
		};
		printf("%f,%f \n", rotatedGain.real, rotatedGain.imaginary);
	}


	printf(">>> INFO: GAIN GALIBRATION operations complete, exiting...\n\n");

	return EXIT_SUCCESS;
}
