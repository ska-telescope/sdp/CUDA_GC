
// Copyright 2019 Adam Campbell, Seth Hall, Andrew Ensor
// Copyright 2019 High Performance Computing Research Laboratory, Auckland University of Technology (AUT)

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:

// 1. Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.

// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution.

// 3. Neither the name of the copyright holder nor the names of its
// contributors may be used to endorse or promote products derived from this
// software without specific prior written permission.

// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include <stdio.h>
#include <stdlib.h>
#include <float.h>
#include <math.h>

#include <cuda_runtime.h>

#include "gain_calibration.h"

//IMPORTANT: Modify configuration for target GPU and DFT
void init_config(Config *config)
{
	// Toggle whether right ascension should be enabled (observation dependant)
	config->enable_right_ascension = true;

	// Number of visibilities per source
	config->num_visibilities = 2000000;

	config->grid_size = 2000;

	// Source of Visibilities
	config->vis_predicted_file    = "../data/sample_predicted_visibilities.csv";

	config->vis_measured_file    = "../data/GLEAM_small_visibilities_corrupted.csv";

	// Fourier domain grid cell size in radians 
	config->cell_size = 6.39708380288950e-6;

	// Frequency of visibility uvw terms
	config->frequency_hz = 100e6;

	// Scalar for visibility coordinates
	config->uv_scale = config->grid_size * config->cell_size;

	// Number of CUDA threads per block - this is GPU specific
	config->gpu_max_threads_per_block = 1024;

	config->num_recievers = 512;

	config->max_calibration_cycles = 10;
}



void calculate_receiver_pairs(Config *config, int2 *receiver_pairs)
{
	int a = 0;
	int b = 1;
	int baselines = (config->num_recievers*(config->num_recievers-1))/2;

	for(int i=0;i<baselines;++i)
	{
		//printf(">>>> CREATING RECEIVER PAIR (%d,%d) \n",a,b);
		receiver_pairs[i].x = a;
		receiver_pairs[i].y = b;

		b++;
		if(b>=config->num_recievers)
		{
			a++;
			b = a+1;
		}
	}

}

void check_cuda_solver_error_aux(const char *file, unsigned line, const char *statement, cusolverStatus_t err)
{
	if (err == cudaSuccess)
		return;

	printf(">>> CUDA ERROR: %s returned %s: %u ",statement, file, line);
	exit(EXIT_FAILURE);
}

void execute_gain_calibration(Config *config, Complex *vis_measured, Complex *vis_predicted, 
								Complex *gains_array, int2 *receiver_pairs)
{

	int baselines = (config->num_recievers*(config->num_recievers-1))/2;

	printf("UPDATE >>> EXECUTING GAIN CALIBRATION for %d recievers with total baselines of %d...\n\n",config->num_recievers, baselines);

	PRECISION2 *device_predicted;
	PRECISION2 *device_measured;
	PRECISION2 *device_gains;
	int2 *device_receiver_pairs;


	CUDA_CHECK_RETURN(cudaMalloc(&device_measured,  sizeof(PRECISION2) * baselines));
	CUDA_CHECK_RETURN(cudaMemcpy(device_measured, vis_measured, baselines * sizeof(PRECISION2), cudaMemcpyHostToDevice));
	cudaDeviceSynchronize();

	CUDA_CHECK_RETURN(cudaMalloc(&device_predicted,  sizeof(PRECISION2) * baselines));
	CUDA_CHECK_RETURN(cudaMemcpy(device_predicted, vis_predicted, baselines * sizeof(PRECISION2), cudaMemcpyHostToDevice));
	cudaDeviceSynchronize();

	CUDA_CHECK_RETURN(cudaMalloc(&device_gains,  sizeof(PRECISION2) * config->num_recievers));
	CUDA_CHECK_RETURN(cudaMemcpy(device_gains, gains_array, config->num_recievers * sizeof(PRECISION2), cudaMemcpyHostToDevice));
	cudaDeviceSynchronize();

	CUDA_CHECK_RETURN(cudaMalloc(&device_receiver_pairs,  sizeof(int2) * baselines));
	CUDA_CHECK_RETURN(cudaMemcpy(device_receiver_pairs, receiver_pairs, baselines * sizeof(int2), cudaMemcpyHostToDevice));
	cudaDeviceSynchronize();


	PRECISION *Q_array;
	PRECISION *A_array;

	//intit Q and A Array for reuse;
	CUDA_CHECK_RETURN(cudaMalloc(&Q_array,  sizeof(PRECISION) * 2 * config->num_recievers));
	CUDA_CHECK_RETURN(cudaMalloc(&A_array,  sizeof(PRECISION) * 2 * config->num_recievers * 2 * config->num_recievers));


	//SET CUDA WORK PLAN:
	int max_threads_per_block = min(config->gpu_max_threads_per_block, baselines);
	int num_blocks = (int) ceil((double) baselines / max_threads_per_block);
	dim3 kernel_blocks(num_blocks, 1, 1);
	dim3 kernel_threads(max_threads_per_block, 1, 1);


	//Set SVD plans
	const int num_rows = 2 * config->num_recievers;
	const int num_cols = 2 * config->num_recievers;


	cusolverDnHandle_t solver_handle;
	cusolverDnCreate(&solver_handle);

	PRECISION *d_U = NULL;
	PRECISION *d_V = NULL;
	PRECISION *d_S = NULL;

	CUDA_CHECK_RETURN(cudaMalloc(&d_U, sizeof(PRECISION) * num_rows * num_cols));
	CUDA_CHECK_RETURN(cudaMalloc(&d_V, sizeof(PRECISION) * num_rows * num_cols));
	CUDA_CHECK_RETURN(cudaMalloc(&d_S, sizeof(PRECISION) * num_rows));

	int work_size = 0;
	CUDA_SOLVER_CHECK_RETURN(SVD_BUFFER_SIZE(solver_handle, num_rows, num_cols, &work_size));
	cudaDeviceSynchronize();

	PRECISION *work;	
	CUDA_CHECK_RETURN(cudaMalloc(&work, work_size * sizeof(PRECISION)));


	PRECISION *d_SUQ = NULL;
	CUDA_CHECK_RETURN(cudaMalloc(&d_SUQ, sizeof(PRECISION) * num_cols));


	for(int i=0;i<config->max_calibration_cycles;++i)
	{
 		//EXECUTE CUDA KERNEL UPDATING Q AND A array (NEED ATOMIC ACCESS!)
		update_gain_calibration<<<kernel_blocks, kernel_threads>>>(
				device_measured,
				device_predicted,
				device_gains,
				device_receiver_pairs,
				A_array,
				Q_array,
				config->num_recievers,
				baselines
		);
		cudaDeviceSynchronize();	

		//NOW DO THE SVD and Gauss-Newton to  Delat and update Gains array (G = G+DELTA)


		if(!execute_calibration_SVD(config, solver_handle, A_array, d_U, d_V, d_S, work, work_size))
		{	printf("UPDATE >>> Unable to calibrate further, S array in SVD found a 0, exiting calibration cycle...\n");
			break;
		}	
		else
		{
			
			int max_threads_per_block = min(config->gpu_max_threads_per_block, num_cols);
			int num_blocks = (int) ceil((double)num_cols / max_threads_per_block);
			dim3 kernel_blocks(num_blocks, 1, 1);
			dim3 kernel_threads(max_threads_per_block, 1, 1);

			printf("UPDATE >>> CALCULATE SUQ PRODUCT... \n");
			calculate_suq_product<<<kernel_blocks, kernel_threads>>>(
				d_S,
				d_U,
				Q_array, 
				d_SUQ, 
				num_cols);
			cudaDeviceSynchronize();

			max_threads_per_block = min(config->gpu_max_threads_per_block, config->num_recievers);
			num_blocks = (int) ceil((double) config->num_recievers / max_threads_per_block);
			dim3 blocks(num_blocks, 1, 1);
			dim3 threads(max_threads_per_block, 1, 1);
			printf("UPDATE >>> CALCULATING DELTA TO UPDATE GAINS ... \n");
			calculate_delta_update_gains<<<blocks, threads>>>(
					d_V,
					d_SUQ,
					device_gains,
					config->num_recievers,
					num_cols
				);
			cudaDeviceSynchronize();
		}

		//RESET ALL MATRICES FOR NEXT CYCLE
		CUDA_CHECK_RETURN(cudaMemset(Q_array, 0, sizeof(PRECISION) * 2 * config->num_recievers));
		CUDA_CHECK_RETURN(cudaMemset(A_array, 0, sizeof(PRECISION) * 2 * config->num_recievers * 2 * config->num_recievers));
		CUDA_CHECK_RETURN(cudaMemset(d_V, 0, sizeof(PRECISION) * num_rows * num_cols));
		CUDA_CHECK_RETURN(cudaMemset(d_U, 0, sizeof(PRECISION)* num_rows * num_cols));
		CUDA_CHECK_RETURN(cudaMemset(d_S, 0, sizeof(PRECISION) * num_rows));
		CUDA_CHECK_RETURN(cudaMemset(work, 0, sizeof(PRECISION) * work_size));
		CUDA_CHECK_RETURN(cudaMemset(d_SUQ, 0, sizeof(PRECISION) * num_cols));
	}

	if (d_SUQ   ) CUDA_CHECK_RETURN(cudaFree(d_SUQ));
	if (d_S     ) CUDA_CHECK_RETURN(cudaFree(d_S));
    if (d_U     ) CUDA_CHECK_RETURN(cudaFree(d_U));
    if (d_V     ) CUDA_CHECK_RETURN(cudaFree(d_V));
    if (work    ) CUDA_CHECK_RETURN(cudaFree(work));
	CUDA_SOLVER_CHECK_RETURN(cusolverDnDestroy(solver_handle));
	CUDA_CHECK_RETURN(cudaFree(Q_array));
	CUDA_CHECK_RETURN(cudaFree(A_array));

	//copy gains back to host FOR NOW - SEPERATE LATER
	CUDA_CHECK_RETURN(cudaMemcpy(gains_array, device_gains, config->num_recievers * sizeof(PRECISION2), cudaMemcpyDeviceToHost));
	cudaDeviceSynchronize();


	//Should freee other memory for now!
	CUDA_CHECK_RETURN(cudaFree(device_predicted));
	CUDA_CHECK_RETURN(cudaFree(device_measured));
	CUDA_CHECK_RETURN(cudaFree(device_gains));
	CUDA_CHECK_RETURN(cudaFree(device_receiver_pairs));
}

__global__ void calculate_suq_product(const PRECISION *d_S, const PRECISION *d_U, const PRECISION *d_Q, PRECISION *d_SUQ, const int num_entries)
{
	//qus 2Nx1, q = 2Nx1 , s=2Nx1, u=2N*2N
	const int index = threadIdx.x + blockDim.x * blockIdx.x;
	if(index >= num_entries)
		return;

	PRECISION sinv = (abs(d_S[index]) > 1E-6) ? 1.0/d_S[index] : 0.0;

	PRECISION product = 0; 
	for(int i=0;i<num_entries;++i)
	{
		product += d_Q[i] * d_U[index*num_entries + i];
	}

	d_SUQ[index] = product * sinv;
}



//delta+= transpose(V)*product of S U and Q (calculated in previous kernel)
__global__ void calculate_delta_update_gains(const PRECISION *d_V, const PRECISION *d_SUQ, PRECISION2 *d_gains, 
													const int num_recievers, const int num_cols)
{
	const int index = threadIdx.x + blockDim.x * blockIdx.x ;

	if(index >= num_recievers)
		return;

	PRECISION delta_top = 0;
	PRECISION delta_bottom = 0;

	int vindex = index * 2;
	for(int i=0;i<num_cols; ++i)
	{
		delta_top += d_SUQ[i] * d_V[vindex*num_cols + i];//[i*num_cols + vindex];
		delta_bottom += d_SUQ[i] * d_V[(vindex+1)*num_cols + i];//[i*num_cols + vindex+1];
	}

	d_gains[index].x += delta_top;
	d_gains[index].y += delta_bottom; 
}



/**
 * Check the return value of the CUDA runtime API call and exit
 * the application if the call has failed.
 */
void check_cuda_error_aux(const char *file, unsigned line, const char *statement, cudaError_t err)
{
	if (err == cudaSuccess)
		return;

	printf(">>> CUDA ERROR: %s returned %s at %s : %u ",statement, file, cudaGetErrorString(err), line);
	exit(EXIT_FAILURE);
}


//Followed this CUDA SVD tutorial - very helpful http://www.orangeowlsolutions.com/archives/2081



bool execute_calibration_SVD(Config *config, cusolverDnHandle_t solver_handle, PRECISION *d_A, 
								PRECISION *d_U, PRECISION *d_V, PRECISION *d_S, 
								PRECISION *work, int work_size)
{
    const int num_rows = 2 * config->num_recievers;
	const int num_cols = 2 * config->num_recievers;

	
	int *devInfo;
	CUDA_CHECK_RETURN(cudaMalloc(&devInfo, sizeof(int)));

	//ALL OUR MATRICES ARE "ROW" MAJOR HOWEVER AS A IS SYMMETRIC DOES NOT NEED TO BE TRANSPOSED FOR FOR SVD ROUTINE
	//SO NEED TO NOT TRANSPOSE U AND TRANSPOSE VSTAR

	CUDA_SOLVER_CHECK_RETURN(SVD(solver_handle, 'A', 'A', num_rows, num_cols, d_A,
		 num_rows, d_S, d_U, num_rows, d_V, num_cols, work, work_size, NULL, devInfo));
	cudaDeviceSynchronize();

	int devInfo_h = 0;	
	CUDA_CHECK_RETURN(cudaMemcpy(&devInfo_h, devInfo, sizeof(int), cudaMemcpyDeviceToHost));
	cudaDeviceSynchronize();

	//CHECKING S PRODUCT!!
    bool success = (devInfo_h == 0);
	printf("UPDATE >>> SVD complete...\n\n");
	if (devInfo ) CUDA_CHECK_RETURN(cudaFree(devInfo));
	return success;
}



__global__ void update_gain_calibration(const PRECISION2 *vis_measured_array, const PRECISION2 *vis_predicted_array, 
	const PRECISION2 *gains_array, const int2 *receiver_pairs, PRECISION *A_array, PRECISION *Q_array, const int num_recievers, const int num_baselines)
{
	const int index = threadIdx.x + blockDim.x * blockIdx.x;
	if(index >= num_baselines)
		return;

	PRECISION2 vis_measured = vis_measured_array[index];
	PRECISION2 vis_predicted = vis_predicted_array[index];

	int2 antennae = receiver_pairs[index];

	PRECISION2 gainA = gains_array[antennae.x];
	//only need gainB as conjugate??
	PRECISION2 gainB_conjugate = complex_conjugate(gains_array[antennae.y]);

	//NOTE do not treat residual as a COMPLEX!!!!! (2 reals)
	PRECISION2 residual = complex_subtract(vis_measured, complex_multiply(vis_predicted,complex_multiply(gainA, gainB_conjugate)));


	//CALCULATE Partial Derivatives

	PRECISION2 part_respect_to_real_gain_a = complex_multiply(vis_predicted, gainB_conjugate);

	PRECISION2 part_respect_to_imag_gain_a = flip_for_i(complex_multiply(vis_predicted, gainB_conjugate));

	PRECISION2 part_respect_to_real_gain_b = complex_multiply(vis_predicted,gainA);

	PRECISION2 part_respect_to_imag_gain_b = flip_for_neg_i(complex_multiply(vis_predicted, gainA));

	//Calculate Q[2a],Q[2a+1],Q[2b],Q[2b+1] arrays - In this order... NEED ATOMIC UPDATE 
	double qValue = part_respect_to_real_gain_a.x * residual.x 
					+ part_respect_to_real_gain_a.y * residual.y;
	atomicAdd(&(Q_array[2*antennae.x]), qValue);

	qValue = part_respect_to_imag_gain_a.x * residual.x 
					+ part_respect_to_imag_gain_a.y * residual.y;
	atomicAdd(&(Q_array[2*antennae.x+1]), qValue);

	qValue = part_respect_to_real_gain_b.x * residual.x 
					+ part_respect_to_real_gain_b.y * residual.y;
	atomicAdd(&(Q_array[2*antennae.y]), qValue);

	qValue = part_respect_to_imag_gain_b.x * residual.x 
					+ part_respect_to_imag_gain_b.y * residual.y;
	atomicAdd(&(Q_array[2*antennae.y+1]), qValue);


	int num_cols = 2 * num_recievers;
	//CALCULATE JAcobian product on A matrix... 2a2a, 2a2a+1, 2a2b, 2a2b+1
	//2a2a
	double aValue = part_respect_to_real_gain_a.x * part_respect_to_real_gain_a.x + 
					part_respect_to_real_gain_a.y * part_respect_to_real_gain_a.y; 
	
	int aIndex = (2 *  antennae.x * num_cols) + (2 * antennae.x);

	atomicAdd(&(A_array[aIndex]), aValue);

	//2a2a+1,
	aValue = part_respect_to_real_gain_a.x * part_respect_to_imag_gain_a.x + 
					part_respect_to_real_gain_a.y * part_respect_to_imag_gain_a.y; 

	aIndex = (2 *  antennae.x * num_cols) + (2 * antennae.x + 1);

	atomicAdd(&(A_array[aIndex]), aValue);

	//2a2b
	aValue = part_respect_to_real_gain_a.x * part_respect_to_real_gain_b.x + 
					part_respect_to_real_gain_a.y * part_respect_to_real_gain_b.y;

	aIndex = (2 *  antennae.x * num_cols) + (2 * antennae.y);

	atomicAdd(&(A_array[aIndex]), aValue);

	//2a2b+1
	aValue = part_respect_to_real_gain_a.x * part_respect_to_imag_gain_b.x + 
					part_respect_to_real_gain_a.y * part_respect_to_imag_gain_b.y;

	aIndex = (2 *  antennae.x * num_cols) + (2 * antennae.y+1);

	atomicAdd(&(A_array[aIndex]), aValue);
	//CACLUATE JAcobian product on A matrix... [2a+1,2a], [2a+1,2a+1], [2a+1,2b], [2a+1,2b+1]
	//2a+1,2a
	aValue = part_respect_to_imag_gain_a.x * part_respect_to_real_gain_a.x + 
					part_respect_to_imag_gain_a.y * part_respect_to_real_gain_a.y; 

	aIndex = ((2 *  antennae.x+1) * num_cols) + (2 * antennae.x);

	atomicAdd(&(A_array[aIndex]), aValue);

	//2a+1,2a+1
	aValue = part_respect_to_imag_gain_a.x * part_respect_to_imag_gain_a.x + 
					part_respect_to_imag_gain_a.y * part_respect_to_imag_gain_a.y; 

	aIndex = ((2 *  antennae.x+1) * num_cols) + (2 * antennae.x+1);

	atomicAdd(&(A_array[aIndex]), aValue);

	//2a+1,2b
	aValue = part_respect_to_imag_gain_a.x * part_respect_to_real_gain_b.x + 
					part_respect_to_imag_gain_a.y * part_respect_to_real_gain_b.y;

	aIndex = ((2 *  antennae.x+1) * num_cols) + (2 * antennae.y);

	atomicAdd(&(A_array[aIndex]), aValue);

	//2a+1,2b+1
	aValue = part_respect_to_imag_gain_a.x * part_respect_to_imag_gain_b.x + 
					part_respect_to_imag_gain_a.y * part_respect_to_imag_gain_b.y;

	aIndex = ((2 *  antennae.x+1) * num_cols) + (2 * antennae.y+1);

	atomicAdd(&(A_array[aIndex]), aValue);


	//CACLUATE JAcobian product on A matrix... [2b,2a], [2b,2a+1], [2b,2b], [2b,2b+1]
	//2b,2a
	aValue = part_respect_to_real_gain_b.x * part_respect_to_real_gain_a.x + 
					part_respect_to_real_gain_b.y * part_respect_to_real_gain_a.y; 
	
	aIndex = (2 *  antennae.y * num_cols) + (2 * antennae.x);

	atomicAdd(&(A_array[aIndex]), aValue);

	//2b,2a+1
	aValue = part_respect_to_real_gain_b.x * part_respect_to_imag_gain_a.x + 
			 		part_respect_to_real_gain_b.y * part_respect_to_imag_gain_a.y;

	aIndex = (2 *  antennae.y * num_cols) + (2 * antennae.x+1);

	atomicAdd(&(A_array[aIndex]), aValue);
	
	//2b,2b
	aValue = part_respect_to_real_gain_b.x * part_respect_to_real_gain_b.x + 
					part_respect_to_real_gain_b.y * part_respect_to_real_gain_b.y;
	
	aIndex = (2 *  antennae.y * num_cols) + (2 * antennae.y);

	atomicAdd(&(A_array[aIndex]), aValue);


	//2b, 2b+1
	aValue = part_respect_to_real_gain_b.x * part_respect_to_imag_gain_b.x + 
					part_respect_to_real_gain_b.y * part_respect_to_imag_gain_b.y;

	aIndex = (2 *  antennae.y * num_cols) + (2 * antennae.y + 1);

	atomicAdd(&(A_array[aIndex]), aValue);

	//CALCULATE JAcobian product on A matrix... [2b+1,2a], [2b+1,2a+1], [2b+1,2b], [2b+1,2b+1]
	//2b+1,2a
	aValue =  part_respect_to_imag_gain_b.x * part_respect_to_real_gain_a.x + 
					part_respect_to_imag_gain_b.y * part_respect_to_real_gain_a.y; 

	aIndex = ((2 *  antennae.y+1) * num_cols) + (2 * antennae.x);

	atomicAdd(&(A_array[aIndex]), aValue);


	//2b+1,2a+1
	aValue =  part_respect_to_imag_gain_b.x * part_respect_to_imag_gain_a.x+ 
					 part_respect_to_imag_gain_b.y * part_respect_to_imag_gain_a.y;

	aIndex = ((2 *  antennae.y+1) * num_cols) + (2 * antennae.x+1);

	atomicAdd(&(A_array[aIndex]), aValue);


	//2b+1,2b
	aValue =  part_respect_to_imag_gain_b.x * part_respect_to_real_gain_b.x+ 
					part_respect_to_imag_gain_b.y * part_respect_to_real_gain_b.y;

	aIndex = ((2 *  antennae.y+1) * num_cols) + (2 * antennae.y);

	atomicAdd(&(A_array[aIndex]), aValue);

	//2b+1, 2b+1
	aValue = part_respect_to_imag_gain_b.x * part_respect_to_imag_gain_b.x + 
					part_respect_to_imag_gain_b.y * part_respect_to_imag_gain_b.y; 

	aIndex = ((2 *  antennae.y+1) * num_cols) + (2 * antennae.y+1);

	atomicAdd(&(A_array[aIndex]), aValue);

}




__device__ PRECISION2 flip_for_i(const PRECISION2 z)
{
	return MAKE_PRECISION2(-z.y, z.x);
}

__device__ PRECISION2 flip_for_neg_i(const PRECISION2 z)
{
	return MAKE_PRECISION2(z.y, -z.x);
}


// __device__ PRECISION2 partial_respect_to_real_gain_a(const PRECISION2 v, const PRECISION2 g)
// {
// 	return complex_multiply()

// }


//(a+ib)(c+id) = (ac-bd)+i(ad+bc)
__device__ PRECISION2 complex_multiply(const PRECISION2 z1, const PRECISION2 z2)
{	return MAKE_PRECISION2(z1.x * z2.x - z1.y * z2.y, z1.x * z2.y + z1.y * z2.x);
}

// http://mathworld.wolfram.com/ComplexDivision.html
__device__ PRECISION2 complex_divide(const PRECISION2 z1, const PRECISION2 z2)
{
    PRECISION a = z1.x * z2.x + z1.y * z2.y;
    PRECISION b = z1.y * z2.x - z1.x * z2.y;
    PRECISION c = z2.x * z2.x + z2.y * z2.y;
    return MAKE_PRECISION2(a / c, b / c);
}

__device__ PRECISION2 complex_subtract(const PRECISION2 z1, const PRECISION2 z2)
{
    return MAKE_PRECISION2(z1.x - z2.x, z1.y - z2.y);
}

__device__ PRECISION2 complex_conjugate(const PRECISION2 z1)
{
    return MAKE_PRECISION2(z1.x, -z1.y);
}

void load_visibilities(Config *config, Complex **vis, bool vis_measured)
{
		
	printf(">>> UPDATE: Using Visibilities from file...\n\n");

	FILE *file = fopen((vis_measured)?config->vis_measured_file:config->vis_predicted_file, "r");
	if(file == NULL)
	{
		printf(">>> ERROR: Unable to locate visibilities file...\n\n");
		return;
	}
	// Reading in the counter for number of visibilities
	fscanf(file, "%d\n", &(config->num_visibilities));

	*vis=  (Complex*) calloc(config->num_visibilities, sizeof(Complex));

	// File found, but was memory allocated?
	if(*vis == NULL)
	{
		printf(">>> ERROR: Unable to allocate memory for visibilities...\n\n");
		if(file) fclose(file);
		if(*vis) free(*vis);
		return;
	}
	// Used to scale visibility coordinates from wavelengths
	// to meters
	// Read in n number of visibilities
	PRECISION brightReal = 0.0;
	PRECISION brightImag = 0.0;
	PRECISION u = 0.0;
	PRECISION v = 0.0;
	PRECISION w = 0.0;
	PRECISION intensity = 0.0;
	for(int vis_indx = 0; vis_indx < config->num_visibilities; ++vis_indx)
	{
		// Read in provided visibility attributes
		// u, v, w, brightness (real), brightness (imag), intensity
#if SINGLE_PRECISION
		fscanf(file, "%f %f %f %f %f %f\n", &u, &v, &w, 
			&(brightReal), &(brightImag), &intensity);
#else
		fscanf(file, "%lf %lf %lf %lf %lf %lf\n", &u, &v, &w, 
			&(brightReal), &(brightImag), &intensity);
#endif

		(*vis)[vis_indx] = (Complex) {     //OR USE INTENSITY LATER??!!!!
			.real = brightReal * 1.0,
			.imaginary = brightImag * 1.0
		};
	}

	// Clean up
	fclose(file);
		printf(">>> UPDATE: Successfully loaded %d visibilities from file...\n\n",config->num_visibilities);
}
